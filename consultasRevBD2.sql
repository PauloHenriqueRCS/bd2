USE REVBD1;
/*SET SQL_SAFE_UPDATES = 1;*/

/*1.	Alterar o tipo do aluno de ‘Silva’ para segundo ano.*/
UPDATE ALUNO SET TIPO_ALUNO = 2 WHERE NOME = 'Silva' LIMIT 1;

/*2.	Criar outra turma para a disciplina Banco de dados para este semestre.*/
INSERT INTO TURMA VALUE
(123,'CC3380','Primeiro',2017,'Cristiane');

/*3.	Inserir uma nota ‘A’ para ‘Silva’ na turma ‘Banco de dados’ do último semestre.*/
INSERT INTO HISTORICO_ESCOLAR VALUE 
(17,123,'A');

/*4.	Recuperar uma lista de todas as disciplinas e notas de ‘Silva’.*/
SELECT  A.NOME, D.NOME_DISCIPLINA, HE.NOTA FROM 
ALUNO A INNER JOIN HISTORICO_ESCOLAR HE ON A.NUMERO_ALUNO = HE.NUMERO_ALUNO
INNER JOIN DISCIPLINA D INNER JOIN TURMA T ON T.IDTURMA = HE.IDTURMA AND D.NUMERO_DISCIPLINA = T.NUMERO_DISCIPLINA  WHERE A.NOME = 'Silva';

/*5.	Listar os nomes dos alunos que realizaram a disciplina ‘Estrutura de dados’ oferecida no segundo semestre de 2016.*/
SELECT  A.NOME FROM 
ALUNO A INNER JOIN HISTORICO_ESCOLAR HE ON A.NUMERO_ALUNO = HE.NUMERO_ALUNO 
INNER JOIN DISCIPLINA D INNER JOIN TURMA T ON T.IDTURMA = HE.IDTURMA AND D.NUMERO_DISCIPLINA = T.NUMERO_DISCIPLINA 
WHERE T.SEMESTRE = 'Segundo' AND T.ANO = 2016 AND D.NOME_DISCIPLINA = 'Banco de dados';

/*6.	Recupere os nomes de todos os alunos que estão formando em Introdução a Ciência da Computação.*/
SELECT  A.NOME FROM 
ALUNO A INNER JOIN HISTORICO_ESCOLAR HE ON A.NUMERO_ALUNO = HE.NUMERO_ALUNO 
INNER JOIN DISCIPLINA D INNER JOIN TURMA T ON T.IDTURMA = HE.IDTURMA AND D.NUMERO_DISCIPLINA = T.NUMERO_DISCIPLINA 
WHERE T.SEMESTRE = 'Segundo'  AND D.NOME_DISCIPLINA = 'Introdução a Ciência da Computação';

/*7.	Recupere os nomes de todas as disciplinas lecionadas pelo professor Kleber em 2015 e 2016.*/
SELECT D.NOME_DISCIPLINA FROM DISCIPLINA D INNER JOIN TURMA T ON T.NUMERO_DISCIPLINA = D.NUMERO_DISCIPLINA WHERE T.ANO = 2015 AND 2016 AND T.PROFESSOR = 'Kleber';

/*8.	Para cada matéria lecionada pelo professor Kleber, recupere o número da disciplina, semestre, ano e número de alunos que realizaram a matéria.*/
SELECT  D.NUMERO_DISCIPLINA, T.SEMESTRE, T.ANO , COUNT((HE.NUMERO_ALUNO)) FROM DISCIPLINA D INNER JOIN TURMA T ON T.NUMERO_DISCIPLINA = D.NUMERO_DISCIPLINA
INNER JOIN HISTORICO_ESCOLAR HE ON HE.IDTURMA = T.IDTURMA WHERE T.PROFESSOR = 'Kleber';

/*9.	Recupere o nome e o histórico de cada aluno. O Histórico inclui nome da disciplina, 
número da disciplina, crédito, semestre, ano e nota para cada disciplina feita pelo aluno.*/
SELECT  A.NOME, D.NOME_DISCIPLINA, D.NUMERO_DISCIPLINA, D.CREDITOS, T.SEMESTRE,  T.ANO, HE.NOTA
FROM ALUNO A INNER JOIN  HISTORICO_ESCOLAR HE ON A.NUMERO_ALUNO = HE.NUMERO_ALUNO INNER JOIN TURMA T ON T.IDTURMA = HE.IDTURMA
INNER JOIN DISCIPLINA D ON D.NUMERO_DISCIPLINA = T.NUMERO_DISCIPLINA;

/*SELECTS*/
SELECT * FROM ALUNO;
SELECT * FROM DISCIPLINA;
SELECT * FROM TURMA;
SELECT * FROM HISTORICO_ESCOLAR;
