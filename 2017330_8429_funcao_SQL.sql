create schema funcao;
use funcao;

DELIMITER $$

CREATE FUNCTION palavra (texto CHAR(20)) RETURNS CHAR(50)
Begin
	RETURN CONCAT('Voce digitou: ',texto,' !!!');

end$$
DELIMITER ;

SELECT palavra('oi');


CREATE TABLE notas(aluno VARCHAR(10), nota1 INT, nota2 INT, nota3 INT, nota4 INT);

INSERT INTO notas VALUES('Maria', 10, 9, 10, 10);
INSERT INTO notas VALUES('Pedro', 5, 2, 3, 4);

select * from notas;

DELIMITER $$

CREATE FUNCTION media (nome VARCHAR(10)) RETURNS FLOAT
Begin
	DECLARE n1,n2,n3,n4 INT;
    DECLARE med FLOAT;
    SELECT nota1,nota2,nota3,nota4 INTO n1,n2,n3,n4 FROM notas WHERE aluno = nome;
    SET med = (n1+n2+n3+n4)/4;
    RETURN med;

end$$
DELIMITER ;

SELECT media('Maria');
SELECT media('Pedro');

call media('Pedro');

drop function media;