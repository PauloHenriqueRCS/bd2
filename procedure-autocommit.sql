drop schema sys;
create schema sys;
use sys;

CREATE TABLE TABLE1(
SALARIO DECIMAL,
TIPO INT
);

CREATE TABLE TABLE2(
RESUMO DECIMAL,
TIPO INT
);

INSERT INTO  TABLE1 VALUES
(3000, 1),
(4000, 1),
(3000, 2);

INSERT INTO TABLE2 VALUES 
(0, 1);

start transaction;

select @soma:=sum(salario) from table1 where tipo = 1;
update table2 set resumo = @soma limit 1;

commit;

select @soma;
select * from table2;

insert into table1 values 
(3000, 2);
insert into table2 values 
(0, 2);
set @soma =0;

start transaction;
select @soma:=sum(salario) from table1 where tipo =2;
update table2 set resumo = @soma where tipo = 2 limit 1;

select * from table2;
rollback;

drop procedure proc;

DELIMITER $
create procedure proc()
begin
start transaction;
select @soma:=sum(salario) from table1 where tipo = 2;

if @soma <= 0 then
	rollback;
else
	update table2 set resumo=@soma where tipo = 2;
	commit;
end if;
end$$
DELIMITER ;

select * from table2;

call proc;

select @@autocommit;
drop table testetrans;
create table testetrans(
a int not null
);

insert into testetrans set a = 1;
select * from testetrans;

set @@autocommit = 0;
insert into testetrans set a = 2;
select * from testetrans;
rollback;

select * from testetrans;

DELIMITER $
CREATE PROCEDURE teste()
BEGIN
SELECT 'hello';
END$$
DELIMITER ;

call teste;

drop procedure teste2;
DELIMITER $
CREATE PROCEDURE teste2(IN quantidade INT)
BEGIN
SELECT * FROM table1 LIMIT quantidade;
END$$
DELIMITER ;

call teste2(2);

drop procedure teste3;
DELIMITER $
CREATE PROCEDURE teste3(OUT quantidade INT)
BEGIN
SELECT COUNT(*) INTO quantidade FROM table1;
END$$
DELIMITER ;

select * from table1;
call teste3(@quantidade);
select @quantidade;

drop procedure teste4;
DELIMITER $
CREATE PROCEDURE teste4(INOUT numero INT)
BEGIN
SET numero = numero * numero;
END$$
DELIMITER ;

SET @valor = 5;
call teste4(@valor);
select @valor;

drop procedure teste5;
DELIMITER $
CREATE PROCEDURE teste5()
BEGIN
declare x int;
declare y float;
SET y = 1.1;
SELECT COUNT(*) INTO x FROM table2;
SELECT x, y;
END$$
DELIMITER ;

call teste5;

SET @t1=1, @t2=2, @t3=4;
SELECT @t1, @t2, @t3, @t4 := @t+@t2+@t3;

drop procedure teste6;
DELIMITER $
CREATE PROCEDURE teste6(IN a INT)
BEGIN
declare x float;
IF (a >=0) then 
set x=10;
else 
set x=20;
end IF;
select x;
END$$
DELIMITER ;

call teste6(1);

drop procedure teste7;
DELIMITER $
CREATE PROCEDURE teste7(IN A INT, B INT)
BEGIN
IF (A > B) THEN 
SELECT 'VALOR A MAIOR';
ELSEIF (A < B) THEN 
SELECT 'VALOR A MENOR';
ELSE 
SELECT 'MESMO VALOR';
END IF;
SELECT X;
END$$
DELIMITER ;

call teste7(2,2);

drop procedure teste8;
DELIMITER $
CREATE PROCEDURE teste8()
BEGIN
DECLARE X INT DEFAULT 0;
DECLARE STR VARCHAR(255) DEFAULT '';
SET X = 1;
WHILE (X <= 5) DO
SET STR = CONCAT(STR,X,',');
SET X = X + 1;
END WHILE;
SELECT STR;
END$$
DELIMITER ;

call teste8();

drop procedure teste9;
DELIMITER $
CREATE PROCEDURE teste9()
BEGIN
DECLARE X INT DEFAULT 0;
DECLARE STR VARCHAR(255) DEFAULT '';
SET X = 5;
REPEAT 
SET STR = CONCAT(STR,X,',');
SET X = X - 1;
UNTIL X <= 0
END REPEAT;
SELECT STR;
END$$
DELIMITER ;

call teste9();

DROP PROCEDURE TESTE10;
DELIMITER $
CREATE PROCEDURE TESTE10()
BEGIN
DECLARE STR VARCHAR(255) DEFAULT '';
DECLARE X INT DEFAULT 0;
SET X = -5;
LOOP_LABEL: LOOP
IF X > 0 THEN
LEAVE LOOP_LABEL;
END IF;
SET STR = CONCAT(STR,X,',');
SET X = X + 1;
ITERATE LOOP_LABEL;
END LOOP;
SELECT STR;
END$$
DELIMITER ;

call teste10;





