
-- -----------------------------------------------------
-- Schema notefood
-- -----------------------------------------------------
drop schema notefood;
CREATE SCHEMA IF NOT EXISTS `notefood` DEFAULT CHARACTER SET utf8 ;
USE `notefood` ;

-- -----------------------------------------------------
-- Table `notefood`.`USUARIO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `notefood`.`USUARIO` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(64) NOT NULL,
  `DataNascimento` DATE NOT NULL,
  `Sexo` CHAR(1) NOT NULL,
  `Peso` FLOAT UNSIGNED NOT NULL,
  `Altura` FLOAT UNSIGNED NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `USUARIO_ID_UNIQUE` (`Id` ASC),
  UNIQUE INDEX `Email_UNIQUE` (`Email` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `notefood`.`TIPO_REFEICAO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `notefood`.`TIPO_REFEICAO` (
  `Id` INT NOT NULL,
  `Descricao` VARCHAR(45) NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `notefood`.`REFEICAO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `notefood`.`REFEICAO` (
  `Id` INT UNSIGNED NOT NULL,
  `Data` DATE NOT NULL,
  `TIPO_REFEICAO_Id` INT NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `fk_REFEICAO_TIPO_REFEICAO1_idx` (`TIPO_REFEICAO_Id` ASC),
  CONSTRAINT `fk_REFEICAO_TIPO_REFEICAO1`
    FOREIGN KEY (`TIPO_REFEICAO_Id`)
    REFERENCES `notefood`.`TIPO_REFEICAO` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `notefood`.`GRUPO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `notefood`.`GRUPO` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(45) NOT NULL,
  `Nutriente` VARCHAR(45) NOT NULL COMMENT 'Descreve o nutriente principal do Grup: ex.:\n- Energéticos: carboidratos\n- Reguladores: fibras\n- Construtores: proteína\n- Energéticos-extra: açúcares',
  `LimiteNutriente` FLOAT NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `GRUPO_ID_UNIQUE` (`Id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `notefood`.`SUB_GRUPO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `notefood`.`SUB_GRUPO` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(45) NOT NULL,
  `LimiteSubGrupo` FLOAT NOT NULL,
  `GRUPO_Id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `SUB_GRUPO_ID_UNIQUE` (`Id` ASC),
  INDEX `fk_SUB_GRUPO_GRUPO1_idx` (`GRUPO_Id` ASC),
  CONSTRAINT `fk_SUB_GRUPO_GRUPO1`
    FOREIGN KEY (`GRUPO_Id`)
    REFERENCES `notefood`.`GRUPO` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `notefood`.`ALIMENTO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `notefood`.`ALIMENTO` (
  `Id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(45) NOT NULL,
  `Descricao` VARCHAR(100) NOT NULL,
  `Porcao` VARCHAR(45) NOT NULL,
  `ValorPorcao` FLOAT NOT NULL,
  `SUB_GRUPO_Id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `ALIMENTO_ID_UNIQUE` (`Id` ASC),
  UNIQUE INDEX `Nome_UNIQUE` (`Nome` ASC),
  INDEX `fk_ALIMENTO_SUB_GRUPO1_idx` (`SUB_GRUPO_Id` ASC),
  CONSTRAINT `fk_ALIMENTO_SUB_GRUPO1`
    FOREIGN KEY (`SUB_GRUPO_Id`)
    REFERENCES `notefood`.`SUB_GRUPO` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `notefood`.`ALIMENTO_SELECIONADO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `notefood`.`ALIMENTO_SELECIONADO` (
  `USUARIO_Id` INT UNSIGNED NOT NULL,
  `ALIMENTO_Id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`USUARIO_Id`, `ALIMENTO_Id`),
  INDEX `fk_ALIMENTO_SELECIONADO_USUARIO1_idx` (`USUARIO_Id` ASC),
  INDEX `fk_ALIMENTO_SELECIONADO_ALIMENTO1_idx` (`ALIMENTO_Id` ASC),
  CONSTRAINT `fk_ALIMENTO_SELECIONADO_USUARIO1`
    FOREIGN KEY (`USUARIO_Id`)
    REFERENCES `notefood`.`USUARIO` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ALIMENTO_SELECIONADO_ALIMENTO1`
    FOREIGN KEY (`ALIMENTO_Id`)
    REFERENCES `notefood`.`ALIMENTO` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `notefood`.`REFEICAO_ALIMENTO_SELECIONADO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `notefood`.`REFEICAO_ALIMENTO_SELECIONADO` (
  `ALIMENTO_SELECIONADO_USUARIO_Id` INT UNSIGNED NOT NULL,
  `ALIMENTO_SELECIONADO_ALIMENTO_Id` INT UNSIGNED NOT NULL,
  `QuantidadeAlimento` FLOAT NULL,
  `REFEICAO_Id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`ALIMENTO_SELECIONADO_USUARIO_Id`, `ALIMENTO_SELECIONADO_ALIMENTO_Id`, `REFEICAO_Id`),
  INDEX `fk_REFEICAO_has_ALIMENTO_SELECIONADO_ALIMENTO_SELECIONADO1_idx` (`ALIMENTO_SELECIONADO_USUARIO_Id` ASC, `ALIMENTO_SELECIONADO_ALIMENTO_Id` ASC),
  INDEX `fk_REFEICAO_ALIMENTO_SELECIONADO_REFEICAO1_idx` (`REFEICAO_Id` ASC),
  CONSTRAINT `fk_REFEICAO_has_ALIMENTO_SELECIONADO_ALIMENTO_SELECIONADO1`
    FOREIGN KEY (`ALIMENTO_SELECIONADO_USUARIO_Id` , `ALIMENTO_SELECIONADO_ALIMENTO_Id`)
    REFERENCES `notefood`.`ALIMENTO_SELECIONADO` (`USUARIO_Id` , `ALIMENTO_Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_REFEICAO_ALIMENTO_SELECIONADO_REFEICAO1`
    FOREIGN KEY (`REFEICAO_Id`)
    REFERENCES `notefood`.`REFEICAO` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `notefood`.`TIPO_REFEICAO_ALIMENTO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `notefood`.`TIPO_REFEICAO_ALIMENTO` (
  `TIPO_REFEICAO_Id` INT NOT NULL,
  `ALIMENTO_Id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`TIPO_REFEICAO_Id`, `ALIMENTO_Id`),
  INDEX `fk_TIPO_REFEICAO_has_ALIMENTO_ALIMENTO1_idx` (`ALIMENTO_Id` ASC),
  INDEX `fk_TIPO_REFEICAO_has_ALIMENTO_TIPO_REFEICAO1_idx` (`TIPO_REFEICAO_Id` ASC),
  CONSTRAINT `fk_TIPO_REFEICAO_has_ALIMENTO_TIPO_REFEICAO1`
    FOREIGN KEY (`TIPO_REFEICAO_Id`)
    REFERENCES `notefood`.`TIPO_REFEICAO` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TIPO_REFEICAO_has_ALIMENTO_ALIMENTO1`
    FOREIGN KEY (`ALIMENTO_Id`)
    REFERENCES `notefood`.`ALIMENTO` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `notefood`.`USUARIO` - Inserir o usuário administrador
-- -----------------------------------------------------
INSERT INTO `notefood`.`USUARIO`(`Nome`, `Email`, `DataNascimento`, `Sexo`, `Peso`, `Altura`) 
VALUES('Administrador', 'admin@admin.com', '1900-01-01', 'X', 1000.0, 3);

-- -----------------------------------------------------
-- Table `notefood`.`GRUPO` - Inserir os grupos
-- -----------------------------------------------------
INSERT INTO `notefood`.`GRUPO`(`Nome`, `Nutriente`, `LimiteNutriente`) VALUES
('Energéticos', 'Carboidratos', 9.0),
('Reguladores', 'Fibras', 10.0),
('Construtores', 'Proteínas', 6.0),
('Energéticos-extra', 'Calorias', 4.0);

-- -----------------------------------------------------
-- Table `notefood`.`GRUPO` - Inserir os grupos
-- -----------------------------------------------------
INSERT INTO `notefood`.`SUB_GRUPO`(`Nome`, `LimiteSubGrupo`, `GRUPO_id`) VALUES
('Cereais',2,1),
('Paes',3,1),
('Massas',2,1),
('Tuberculos',2,1),
('Raizes',3,1),
('Hortaliças',5,2),
('Frutas',5,2),
('Leguminosas',2,3),
('Laticínios',3,3),
('Carnes_Ovos',2,3),
('Oleos_Gorduras',2,4),
('Açucares_Doces',1,4);

